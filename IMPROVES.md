[CHECK_MANUAL]: Tutorials/CHECK_MANUAL.md
[CHECK_AUTO]: Tutorials/CHECK_AUTO.md
[CHECK_SERVER]: Tutorials/CHECK_SERVER.md
[PROCESS]: Tutorials/PROCESS.md
[RELAY_MULTI]: Tutorials/RELAY_MULTI.md
[IPFS_DEEP]: Tutorials/IPFS_DEEP.md
[RELAY_NET]: Tutorials/RELAY_NET.md

[ACTUAL_CODE]: https://gitlab.com/aitoribanez/dtailor-stamp
[PAST_CODE]: https://github.com/dTailorg
[PAST_VUE]: https://gitlab.com/aitoribanez/dtailorg-front-victor
[PAST_REACT]: https://github.com/dTailorg/6_App
[DONOSTI_PHP]: https://

[IMPROVES]: IMPROVES.md
[LICENSE]: LICENSE
[README]: README.md

[DAPPWALL]: https://github.com/Colm3na/DAppWall

[<== Vuelta al Inicio][README]
# Mejoras previstas para CriptoRelay

## v2 [Versión actual][ACTUAL_CODE] Integridad temporal y estructural
__Performance__
- Cambiar wallet de bitcoin
- Tabla records ordenada por Timestamp
- Control para cambiar de address
- Dividir en varios servidores micro
- Que los Relays NO hagan la tx a blockchain si no hay nuevas entradas desde la última tx
- Que el nodo de IPFS guarde un máximo de versiones del mismo Relay (configurable)
- Tx con SegWit
- Endpoints:
	- POST para subir archivos a IPFS directamente y que te devuelva el hash
	- GET tx inmutabilizadas en la baseLayer
	- GET para tomar todos archivos subidos a IPFS con su hash
	- DELETE/REMOVE de borrar contenido (por key)
	- GET ordenado por timestamp o por key (el que falte finalmente)

__Despliegue__
- Hacer despliegue desde un script:
	- Script de preparación de servidor
	- Script de Bitcoin
	- Script de IPFS
	- Script de API
	- Script de terminación de servidor

__Mayor Capacidad__
- Relays week, month, year _(más información en: [Tutorial con múltiples "baseLayers"][RELAY_MULTI])_:
	- Tablas en la BD
		- Week:
		- Month:
		- Year:
	- Conexión con:
		- Bitcoin: como baseLayer
		- IPFS como Almacenamiento P2P
	- Temporización con Cron: para inmutabilización cada 7, 28 y 364 días respectivamente (subida de tabla a Almacenamiento P2P => tx en BaseLayer => guardado en tabla de BD):
	- Endpoints (para cada tabla):
		- GET (key):
		- GET (all):
		- POST (record):
		- POST (IPFS):


- Relay hour _(más información en: [Tutorial con múltiples "baseLayers"][RELAY_MULTI])_:
	- Tabla Hour en la BD: para inmutabilización cada hora
	- Conexión con:
		- Smart Contract de Ethereum: la baseLayer [usando algo similar a DappWall][DAPPWALL]
		- Swarm: como Almacenamiento P2P
	- Temporización con Cron: para inmutabilización cada hora (subida de tabla a Almacenamiento P2P => tx en BaseLayer => guardado en tabla de BD):
	- Endpoints:
		- GET (key):
		- GET (all):
		- POST (record):
		- POST (IPFS):


- Relays hack, virtual y user _(más información en: [Tutorial con múltiples "baseLayers"][RELAY_MULTI])_:
	- Tablas en la BD:
		- Hack: inmutabilización manual a voluntad
		- Virtual: sin inmutabilizar
		- User: de otros usuarios y de otros nodos
	- Conexión con:
		- IPFS: para subida a Almacenamiento distribuido a voluntad para cada tabla
	- Endpoints (para cada tabla):
		- GET (key):
		- GET (all):
		- POST (record):
		- POST (IPFS):


__Nuevos Usos__
- Pagos con Bitcoin

__FrontEnd__
- Explorador de bloques de Bitcoin
- Explorador de IPFS
__________________________
## v3: Procesos en Segundas capas
__Performance__
- Usar solo MongoDB (versión sin SQLite)

__Despliegue__
- Dockerización

__Nuevos Usos__
- Nodo de LN
- Creación de invoce manual
- Creación de invoce manual con comentario
- Creación de invoice repetible
- Creación de invoice automáticamente tras inmutabilización
- Pago de invoices manual
- Pago de invoces automático tras inmutabilización
- Creación de canales
- Creación de conjunto de canales
- Canales ocultos
- Nodos ocultos
- Uso de Tor

__FrontEnd__
- Explorador de bloques de Bitcoin solo para tx de Relays
- Explorador de IPFS con 4 niveles de profundidad
- Explorador de LN
__________________________
## v4: Pruebas de conocimiento cero
__Nuevos Usos__
- zk-Claims:
	- Almacenamiento distribuido de zk-Claims
	- Intercambio de zk
__________________________
__________________________
[<== Vuelta al Inicio][README]
