[CHECK_MANUAL]: Tutorials/CHECK_MANUAL.md
[CHECK_AUTO]: Tutorials/CHECK_AUTO.md
[CHECK_SERVER]: Tutorials/CHECK_SERVER.md
[PROCESS]: Tutorials/PROCESS.md
[RELAY_MULTI]: Tutorials/RELAY_MULTI.md
[IPFS_DEEP]: Tutorials/IPFS_DEEP.md
[RELAY_NET]: Tutorials/RELAY_NET.md

[ACTUAL_CODE]: https://gitlab.com/aitoribanez/dtailor-stamp
[PAST_CODE]: https://github.com/dTailorg
[PAST_VUE]: https://gitlab.com/aitoribanez/dtailorg-front-victor
[PAST_REACT]: https://github.com/dTailorg/6_App
[DONOSTI_PHP]: https://

[IMPROVES]: IMPROVES.md
[LICENSE]: LICENSE
[README]: README.md

[DAPPWALL]: https://github.com/Colm3na/DAppWall

# CriptoRelay

CriptoRelay es un acumulador con integridad criptográfica demostrable para aplicaciones y BBDD

### Tutoriales
- __Básicos:__
	- [Tutorial de comprobación manual][CHECK_MANUAL] de la integridad temporal
	- [Tutorial de comprobación automática][CHECK_AUTO] de la integridad temporal ___EN CONSTRUCCIÓN___
	- [Tutorial de comprobación del servidor][CHECK_SERVER] del estado del deslpliegue
	- [Tutorial del proceso a seguir][PROCESS] para conectar aplicaciones externas
- __Avanzados:__
	- [Tutorial para Redes de Relays][RELAY_NET] de la integridad temporal ___EN CONSTRUCCIÓN___
	- [Tutorial con múltiples "baseLayers"][RELAY_MULTI] para garantizar mejorar la integridad ___PREVISTO___
	- [Tutorial estructuras sobre IPFS][IPFS_DEEP] para ganar profundidad ___PREVISTO___

### Repositorios de código
- __BackEnd:__  
	- v2 [Código actual][ACTUAL_CODE] utilizado con BTC, IPFS y API en Javascript
	- v1 [Versión anterior][PAST_CODE] con ETH, IPFS/Swarm y API en Python
- __FrontEnd:__  
	- v1 [Versión en PHP][DONOSTI_PHP] sencilla app de inmutabilización ___PREVISTO___
	- v0.2 [Versión en Vue][PAST_VUE] comienzo de interfaz sin terminar ___EN CONSTRUCCIÓN___
	- v0.1 [Versión en React][PAST_REACT] comienzo de interfaz sin terminar ___EN CONSTRUCCIÓN___

### [Futuras mejoras][IMPROVES] de CriptoRelay
__________________________
#### [_Proyecto de código abierto bajo licencia GPLv3_][LICENSE]
__________________________
__________________________
