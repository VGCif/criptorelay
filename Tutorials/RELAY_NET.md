[CHECK_MANUAL]: Tutorials/CHECK_MANUAL.md
[CHECK_AUTO]: Tutorials/CHECK_AUTO.md
[CHECK_SERVER]: Tutorials/CHECK_SERVER.md
[PROCESS]: Tutorials/PROCESS.md
[RELAY_MULTI]: Tutorials/RELAY_MULTI.md
[IPFS_DEEP]: Tutorials/IPFS_DEEP.md
[RELAY_NET]: Tutorials/RELAY_NET.md

[ACTUAL_CODE]: https://gitlab.com/aitoribanez/dtailor-stamp
[PAST_CODE]: https://github.com/dTailorg
[PAST_VUE]: https://gitlab.com/aitoribanez/dtailorg-front-victor
[PAST_REACT]: https://github.com/dTailorg/6_App
[DONOSTI_PHP]: https://

[IMPROVES]: IMPROVES.md
[LICENSE]: LICENSE
[README]: README.md

[DAPPWALL]: https://github.com/Colm3na/DAppWall

[<== Vuelta al Inicio][README]
# Red de CriptoRelays
Varios relays controlados por empresas y organizaciones diferentes pueden inmutabilizarse mutuamente, garantizándose entre sí la integridad de los datos (e impidiendo la creación de cadenas paralelas sustitutivas)
__________________________
__________________________
[<== Vuelta al Inicio][README]
