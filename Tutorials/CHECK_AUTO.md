[CHECK_MANUAL]: Tutorials/CHECK_MANUAL.md
[CHECK_AUTO]: Tutorials/CHECK_AUTO.md
[CHECK_SERVER]: Tutorials/CHECK_SERVER.md
[PROCESS]: Tutorials/PROCESS.md
[RELAY_MULTI]: Tutorials/RELAY_MULTI.md
[IPFS_DEEP]: Tutorials/IPFS_DEEP.md
[RELAY_NET]: Tutorials/RELAY_NET.md

[ACTUAL_CODE]: https://gitlab.com/aitoribanez/dtailor-stamp
[PAST_CODE]: https://github.com/dTailorg
[PAST_VUE]: https://gitlab.com/aitoribanez/dtailorg-front-victor
[PAST_REACT]: https://github.com/dTailorg/6_App
[DONOSTI_PHP]: https://

[IMPROVES]: IMPROVES.md
[LICENSE]: LICENSE
[README]: README.md

[DAPPWALL]: https://github.com/Colm3na/DAppWall

[<== Vuelta al Inicio][README]
# Comprobación automática de la inmutabilidad
Consiste, mayormente, en resolver la comprobación manual de forma automática confiando en un nodo de la blockchain escogida como ```"baseLayer"``` para realizar la búsqueda, para ello también se puede utilizar la API de un servicio de terceros (en cuyo caso se confiaría en dicho servicio)

## Existen 2 enfoques posibles según el uso que se le esté dando al CriptoRelay
### Uso externo
<details>
  	<summary>
		<strong>
			a) Para añadir integridad a una DB externa con su propia app
		</strong>
  	</summary><br>
	<details>
		<summary>
			<strong>a.a)</strong> Si somos los <strong>dueños del CriptoRelay</strong> que ha inmutabilizado y podemos usar la API y las plataformas que hay en él
		</summary>
		<strong>==> 1º.-</strong> Obtenemos el hash del dato usando el <code>"privcayHash"</code> en caso de ser un dato privado<br>
		<strong>==> 2º.-</strong> Podemos hacer petición al endpoint GET(key) para comprobar si está en la BD del relays<br>
		<strong>==> 3º.-</strong> Y buscar la siguiente tx de inmutabilización en nuestro nodo de baseLayer
	</details>
	<details>
		<summary>
			<strong>a.b)</strong> Si nos han pasado un <strong>dato de otro CriptoRelay</strong> y queremos comprobarlo desde el nuestro
		</summary>
		<strong>==> 1º.-</strong> Obtenemos el hash del dato usando el <code>"privcayHash"</code> en caso de ser un dato privado<br>
		<strong>==> 2º.-</strong> Buscamos las tx realizadas por el <code>"address"</code><br>
		<strong>==> 3º.-</strong> Obtenemos el bloque actual subido al <code>"p2pStorage"</code><br>
		<strong>==> 4º.-</strong> Buscamos el hash del dato<br>
		<strong>==> 5º.-</strong> Comprobamos si está subido en la siguiente tx temporal
	</details>
</details>

__________________________
### Uso interno
<details>
  	<summary>
		<strong>
			b) Como base sobre la que construir una estructura de datos [EN CONSTRUCCIÓN]
		</strong>
  	</summary>
	<details>
		<summary>
			<strong>b.b)</strong> [EN CONSTRUCCIÓN]
		</summary>
		[PREVISTO]
	</details>
</details>

__________________________
__________________________
[<== Vuelta al Inicio][README]
