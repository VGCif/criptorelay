[CHECK_MANUAL]: Tutorials/CHECK_MANUAL.md
[CHECK_AUTO]: Tutorials/CHECK_AUTO.md
[CHECK_SERVER]: Tutorials/CHECK_SERVER.md
[PROCESS]: Tutorials/PROCESS.md
[RELAY_MULTI]: Tutorials/RELAY_MULTI.md
[IPFS_DEEP]: Tutorials/IPFS_DEEP.md
[RELAY_NET]: Tutorials/RELAY_NET.md

[ACTUAL_CODE]: https://gitlab.com/aitoribanez/dtailor-stamp
[PAST_CODE]: https://github.com/dTailorg
[PAST_VUE]: https://gitlab.com/aitoribanez/dtailorg-front-victor
[PAST_REACT]: https://github.com/dTailorg/6_App
[DONOSTI_PHP]: https://

[IMPROVES]: IMPROVES.md
[LICENSE]: LICENSE
[README]: README.md

[DAPPWALL]: https://github.com/Colm3na/DAppWall

[<== Vuelta al Inicio][README]
# Comprobación manual de la inmutabilidad
### 1. Como usuario que recibes una información inmutabilizada

- Busca la información del nodo que hace de Relay en el Timestamping => InfoNodo

![IMAGEN DE DATO MOSTRADO](/IMAGES/imagen.jpg "Dato mostrado")

- En ella encontrarás la forma que se ha utilizado para conseguir esta inmutabilidad. Junto a ella verás un botón para copiar la información completa

![IMAGEN DE BotónDato](/IMAGES/imagen.jpg "BotónDato")

### 2. Comprobar que hash da la información que te han dado

- Usa el botón copiar para copiar el dato completo

- En la InfoNodo busca que hash se ha utilizado para mantener la información privada => HashPrivacy

![IMAGEN DE HashPrivacy DENTRO DE InfoNodo](/IMAGES/imagen.jpg "HashPrivacy dentro de InfoNodo")

- Busca en internet un “online hash converter” _(ej.: https://hash.online-convert.com/es)_

- Selecciona el mismo algoritmo de hash que HashPrivacy

- Pega el contenido copiado con el BotónDato y ejecuta el algoritmo de hasheo

El “churro” de números y letras que aparece es el hash que da la información que te han suministrado

![IMAGEN DE HASH ONLINE CONVERTER](/IMAGES/Relay_HashConverter.jpg "Hash online converter")

- Guarda dicho hash para después

___NOTA: puedes verificar si el json copiado con BotónDato es igual que la información suministrada usando un explorador de JSON___

![IMAGEN DE EXPLORADOR JSON VS IMAGEN DATO ORIGINAL](/IMAGES/imagen.jpg "Explorador JSON VS imagen dato original")

### 3. Búsqueda de la información inmutabilizada por ese nodo

- En InfoNodo, copia la dirección que se ha encargado de inmutabilizar

![IMAGEN DE Address DENTRO DE InfoNodo](/IMAGES/imagen.jpg "Address dentro de InfoNodo")

- En InfoNodo, busca la blockchain que se ha usado

![IMAGEN DE Blockchain DENTRO DE InfoNodo](/IMAGES/imagen.jpg "Blockchain dentro de InfoNodo")

- A continuación busca un “explorador de bloques” de dicha blockchain _(ej.: https://blockstream.info/ para Bitcoin)_

- Pega el Address del Nodo que inmutabiliza y pulsa buscar. Y aparecerán todas las transacciones efectuadas por dicha dirección

![IMAGEN EXPLORADOR DE BLOQUESo](/IMAGES/Relay_BlockchainExplorer.jpg "Explorador de bloques")

- Pulsa sobre la última transacción realizada

![IMAGEN DE EXPLORADOR + ÚLTIMA TX](/IMAGES/Relay_BlockchainExplorer_Tx.jpg "Explorador con última TX")

- En ella aparecerá un hash que se incluyo en la transacción para inmutabilizarlo, copia la versión ASCII

![IMAGEN DE EXPLORADOR OP_RETURN](/IMAGES/Relay_BlockchainExplorer_Tx_OpReturn.jpg "Explorador + OP_RETURN")

___NOTA: si no viene en ASCII, usa un convertidor HEX a ASCII. PARA IPFS: ASCII, PARA SWARM: HEX___

- Volvemos al InfoNodo y miramos que almacenamiento distribuido se está utilizando => P2Pstorage

![IMAGEN DE Address DENTRO DE InfoNodo](/IMAGES/imagen.jpg "Address dentro de  InfoNodo")

- Buscamos un explorador de dicho almacenamiento distribuido (algunos navegadores ya lo traen) _(ej.: http://dweb.link/ipfs/)_

- Una vez en dicho explorador, pegamos la dirección obtenida (el hash de la transacción de blockchain) _(ej.: http://dweb.link/ipfs/QmSZbWuE2pqwdKkPEChW531GtQNkLsrDj5dwFkvfhMDrSW)_

![IMAGEN DE JSON EN EL NAVEGADOR](/IMAGES/Relay_Navegador.jpg "JSON en el navegador")

Nos aparecerá toda la información inmutabilizada por ese nodo de Timestamping


### 4. Comprobar si el hash se encuentra inmutabilizado

- Pulsamos buscar (en Windows: Ctrl+F)

![IMAGEN DE JSON EN EL NAVEGADOR](/IMAGES/Relay_Navegador_CtrlF.jpg "JSON en el navegador buscando")

- Pegamos el HashDato obtenido en la primera parte y busca si aparece

![IMAGEN DE JSON EN NAVEGADOR + CTRL+F CON DATO REMARCADO](/IMAGES/Relay_Navegador_CtrlF_Hash.jpg "JSON en el navegador con dato remarcado")

Si aparece, es que el dato ha sido inmutabilizado. Esto es así porque la cadena de hashes ha sido obligatoriamente creada con anterioridad a la transacción en la blockchain.


### 5a. Comprobación de fecha y hora (desde el último estado)

Una vez has encontrado el HashDato dentro de todos los registros inmutabilizados

- Ve hasta la siguiente combinación formada por 2 hashes: id tx de blockchain y la dirección de P2Pstorage

![IMAGEN DE JSON EN NAVEGADOR + DATO REMARCADO + NEXT TIMESTAMPING](/IMAGES/Relay_Navegador_Timestamp.jpg "JSON en navegador + dato remarcado + siguiente timestamping")

- Junto a ellos encontrarás la fecha y hora en formato del kernel de Linux tras => Timestamp

___NOTA: Puedes usar un convertidor online para ver la fecha y hora de inmutabilización (ej.: https://www.epochconverter.com/)___

- Pon el id tx en el explorador de bloques de la blockchain utilizado antes para comprobarlo

- La última comprobación es coger todo el contenido que hay hasta la tx de inmutabilización, subirlo al almacenamiento distribuido similar al usado y ver si la dirección concuerda con el hash usado en la tx de inmutabilización.

___NOTA: por cuestiones de almacenamiento es posible que solo se mantenga el último estado del timestamping y no todos los estados previos___


### 5b. Comprobación de fecha y hora (desde su inmutabilización más cercana)

Usando la fecha que te dan en el dato original

- Busca en el explorador de la blockchain la transacción del día siguiente (se inmutabiliza cada 24h)

- Continúa por la parte 3 de este tutorial de forma similar

Si todo funciona correctamente (es posible que dicho estado no esté mantenido online por el P2Pstorage), habrás verificado usando la blockchain que la fecha y hora de inmutabilización es correcta

__________________________
__________________________

# Aclaración final
### Esta comprobación puede parecer difícil y demasiado técnica para que los usuarios la realicen cada vez que reciban una información. Esto es completamente cierto, la realización de hashes y el seguimiento de las cadenas que se forman entre ellos es inviable para una persona, pero increíblemente fácil y seguro para un programa. La información se inmutabiliza con el objetivo de que programas en el futuro puedan comprobar su veracidad y utilizarlo de forma segura, independientemente del lugar (o contenedor) donde se encuentre.
__________________________
__________________________
[<== Vuelta al Inicio][README]
