[CHECK_MANUAL]: Tutorials/CHECK_MANUAL.md
[CHECK_AUTO]: Tutorials/CHECK_AUTO.md
[CHECK_SERVER]: Tutorials/CHECK_SERVER.md
[PROCESS]: Tutorials/PROCESS.md
[RELAY_MULTI]: Tutorials/RELAY_MULTI.md
[IPFS_DEEP]: Tutorials/IPFS_DEEP.md
[RELAY_NET]: Tutorials/RELAY_NET.md

[ACTUAL_CODE]: https://gitlab.com/aitoribanez/dtailor-stamp
[PAST_CODE]: https://github.com/dTailorg
[PAST_VUE]: https://gitlab.com/aitoribanez/dtailorg-front-victor
[PAST_REACT]: https://github.com/dTailorg/6_App
[DONOSTI_PHP]: https://

[IMPROVES]: IMPROVES.md
[LICENSE]: LICENSE
[README]: README.md

[DAPPWALL]: https://github.com/Colm3na/DAppWall

[<== Vuelta al Inicio][README]
# Relay Múltiple
Al relay de inmutabilización diaria se le pueden unir otros relays que trabajan en periodos de tiempo diferente.

Se pueden diferenciar claramente entre 3 grupos:

- __Relays que van más rápido__
	- __Sec__: inmutabilizan cada segundo. Pensado para inmutabilizar maquinaria. Debido a su alta cantidad de registros, se debe buscar una ```"baseLayer"``` gratuíta. _Candidata: IOTA, EOS, TRON_
	- __Min__: inmutabilizan cada minuto. Pensado para inmutabilizar gran cantidad de intercambios por diferentes participantes (por ejemplo en un restaurante). _Candidata: ETH(SC), Aeternity_
	- __Hour__: inmutabilizan cada hora. Pensado para inmutabilizar jornadas laborales. Con smart contract sobre Ethereum. _Candidata: ETH(SC), Aeternity_

- __Relays que van más lento, y sirven para "asegurar" continuidad__
	- __Week__: cada 7 días. Se puede usar la misma que la de día, pues apenas aumenta el coste
	- __Month__: cada 28 días (4 semanas). Se puede usar la misma que la de día, pues apenas aumenta el coste
	- __Year__: cada 364 días (52 semanas o 13 meses de 28 días). Se puede usar la misma que la de día, pues apenas aumenta el coste

- __Y Relays que no inmutabilizan directamente en ninguna ```"baseLayer"```__
	- __User__: con el fin de unir a distintos participantes en un mismo relay
	- __Virtual__: permite subir información a la BD del Relay pero sin que esta sea inmutabilizada
	- __Hack__: se copian en él las copias de seguridad para permitir sustituir la BD del Relay diario, y permite la inmutabilización manual a voluntad

Los relays múltiples no solo aumentan la integridad de los datos, sino que permite una mejor indexación de la información desde el address principal escogido (el ```"address"``` de la```"baseLayer"``` del relay de día usado en el ```"nodeInfo"```)
__________________________
__________________________
[<== Vuelta al Inicio][README]
