[CHECK_MANUAL]: Tutorials/CHECK_MANUAL.md
[CHECK_AUTO]: Tutorials/CHECK_AUTO.md
[CHECK_SERVER]: Tutorials/CHECK_SERVER.md
[PROCESS]: Tutorials/PROCESS.md
[RELAY_MULTI]: Tutorials/RELAY_MULTI.md
[IPFS_DEEP]: Tutorials/IPFS_DEEP.md
[RELAY_NET]: Tutorials/RELAY_NET.md

[ACTUAL_CODE]: https://gitlab.com/aitoribanez/dtailor-stamp
[PAST_CODE]: https://github.com/dTailorg
[PAST_VUE]: https://gitlab.com/aitoribanez/dtailorg-front-victor
[PAST_REACT]: https://github.com/dTailorg/6_App
[DONOSTI_PHP]: https://

[IMPROVES]: IMPROVES.md
[LICENSE]: LICENSE
[README]: README.md

[DAPPWALL]: https://github.com/Colm3na/DAppWall

[<== Vuelta al Inicio][README]
# Comprobaciones y reinicios
Los comandos a utilizar para la comprobación del servidor son:
```shell
htop
ps -ef
reboot
```
Para acabar con procesos:
```shell
kill -9 <nºProceso>
```
Para mantener actualizado el servidor:
```shell
sudo apt update
sudo apt upgrade
```
Para ver que puertos se están usando:
```shell
sudo lsof -i -P -n | grep LISTEN
```
Para modificar los tiempos de inmutabilizar:
```shell
crontab -e
```
__________________________
Los comandos para comprobar Bitcoin:
```shell
bitcoin-cli listunspent           
bitcoin-cli getblockcount
sudo systemctl status bitcoind
sudo systemctl start bitcoind
```
Para IPFS:
```shell
sudo systemctl status ipfs
sudo systemctl start ipfs
```
Para Loopback:
```shell
pm2 stop npm
pm2 start npm --start
```
__________________________
Tx de prueba general:
```shell
cd && cd dtailor-stamp && /home/ubuntu/.nvm/versions/node/v10.9.0/bin/node everyTime.js
```
Reinicar systemctl:
```shell
sudo systemctl daemon-reload
```
See dtailor-stamp logs
```shell
tail -f logs/debug.log
```
__________________________
__________________________
[<== Vuelta al Inicio][README]
