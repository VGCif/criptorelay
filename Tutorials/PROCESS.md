[CHECK_MANUAL]: Tutorials/CHECK_MANUAL.md
[CHECK_AUTO]: Tutorials/CHECK_AUTO.md
[CHECK_SERVER]: Tutorials/CHECK_SERVER.md
[PROCESS]: Tutorials/PROCESS.md
[RELAY_MULTI]: Tutorials/RELAY_MULTI.md
[IPFS_DEEP]: Tutorials/IPFS_DEEP.md
[RELAY_NET]: Tutorials/RELAY_NET.md

[ACTUAL_CODE]: https://gitlab.com/aitoribanez/dtailor-stamp
[PAST_CODE]: https://github.com/dTailorg
[PAST_VUE]: https://gitlab.com/aitoribanez/dtailorg-front-victor
[PAST_REACT]: https://github.com/dTailorg/6_App
[DONOSTI_PHP]: https://

[IMPROVES]: IMPROVES.md
[LICENSE]: LICENSE
[README]: README.md

[DAPPWALL]: https://github.com/Colm3na/DAppWall

[<== Vuelta al Inicio][README]
# Qué debe llevar la interfaz gráfica
Aparte de mostrar la información inmutabilizada, se debe:
- Añadir __botón copiar elemento JSON__ de la informacón inmutabilizada (para su uso en otra parte o para su comprobación)
- Añadir __información de NodeInfo__ (para permitir la comprobación de la información inmutabilizada)
- Añadir __fecha de inmutabilización__ _(si se usa el timestamp diario, la fecha es al día siguiente de su registro)_
- Mostrar opción para contenido __público o privado__ (lo subirá a IPFS o lo hasheará)
__________________________
# Primeros pasos
Lo primero, crearemos un objeto JSON con la información utilizada para inmutabilizar, de esta forma
```JSON
{
	"nodeInfo":{
		"name":"<NAME>",
		"baseLayer":"<BLOCKCHAIN>",
		"address":"<ADDRESS DE DICHA BLOCKCHAIN>",
		"storage":"<ALMACENAMIENTO DISTRIBUIDO>",
		"privacyHash":"<ALGORITMO HASH>",
		"tutorial":"<TUTORIAL>"
	}
}
```
- Como por ejemplo
```JSON
{
	"nodeInfo":{
		"name":"Relay Donosti",
		"baseLayer":"Bitcoin",
		"address":"19Ds3C4t5DeER6pSazRLHXMtNBMTfbXb2U",
		"storage":"IPFS",
		"privacyHash":"sha256",
		"tutorial":"https:\/\/gitlab.com\/VGCif\/criptorelay\/blob\/master\/Tutorials\/CHECK_MANUAL.md"
	}
}
```
__________________________
Subimos a IPFS el contenido de la Información del relay de Timestamping, colocándo el JSON dentro de la carpeta ```/home/ubuntu/dtail-stamp/files```
```shell
```
Obtenemos el hash, introduciendo el nombre del archivo en el endpoint de la API ```IPFS/UserController```  de esta forma:
```shell
curl -X GET "http://<IP>/ipfs/upload/<NOMBRE ARCHIVO>" -H "accept: application/json" -H "Authorization: Bearer <TOKEN AUTH>"
```
__________________________
A partir de ahí, cada vez que queramos subir un dato, unimos el objeto JSON que se quiera subir a el objeto ```"nodeInfo"``` formando un elemento de 2 objetos, tal que así:
```JSON
[
	{
		"<indice escogido>":{<INFORMACIÓN QUE SE DESEE INMUTABILIZAR>}
	},
	{
		"nodeInfo":
		{
			"name":"<NAME>",
			"baseLayer":"<BLOCKCHAIN>",
			"address":"<ADDRESS DE DICHA BLOCKCHAIN>",
			"storage":"<ALMACENAMIENTO DISTRIBUIDO>",
			"privacyHash":"<ALGORITMO HASH>",
			"tutorial":"<TUTORIAL>"
		}
	}
]
```
- O en su forma reducida, utilizando el hash de IPFS donde esta la información de ```"nodeInfo"```, para no llenar la BD del servidor que hace de Relay con más información repetida
```JSON
[
	{
		"<indice escogido>":{<INFORMACIÓN QUE SE DESEE INMUTABILIZAR>}
	},
	{
		"nodeInfo":"<HASH IPFS NODEINFO>"
	}
]
```
__________________________
#### Ahora tendremos 2 opciones:
- __a) Mantener el dato privado:__ para lo que Hasheamos el elemento JSON con el algoritmo resumen escogido en ```"privacyHash"``` obteniendo un hash como resultado  
- __b) Mantener el dato público:__ subiéndolo a IPFS de la misma forma que el JSON de NodeInfo
__________________________
Montamos un objeto JSON con el hash obtenido de cualquiera de las dos formas como "value" y añadimos como "key" un identificador único, así
```JSON
{
	"key":"<UUI>",
	"value":"<HASH OBTENIDO>"
}
```
- Si lo queremos, podemos incluir el timestamp en formato UNIX, aunque no se recomienda, pues supone de entrada una pérdida de la integridad de la inmutabilización anterior. Quedando así:
```JSON
{
	"key":"<UUI>",
	"value":"<HASH OBTENIDO>",
	"timestamp":"<TIEMPO EN FORMATO UNIX>"
}
```
__________________________
Ahora subimos el objeto JSON final ```key:value``` mediante el endpoint de la API llamado ```POST/records```
```shell
curl -X POST "http://<IP DEL SERVIDOR>/records" -H "accept: application/json" -H "Authorization: Bearer <TOKEN AUTH>" -H "Content-Type: application/json" -d "{\"key\":\"<UUI>\",\"value\":\"<HASH OBTENIDO>\",\"timestamp\":<TIEMPO EN FORMTAO UNIX>}"
```
__________________________
__________________________
[<== Vuelta al Inicio][README]
